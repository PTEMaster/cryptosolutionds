//
//  SideVC.swift
//  Auction
//
//  Created by mac on 06/01/21.
//

import UIKit

class SideVC: UIViewController {

    //-------------------------------------------------
    //MARK:- IBOutlet
    //-------------------------------------------------
    @IBOutlet weak var imgProfile: UIImageView!
    
   // weak var delegate:onDissmissDelegate?
    
    //-------------------------------------------------
    //MARK:- Life cycle
    //-------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
       /* if AppDataManager.shared.isLogin() {
            setProfile()
        } else {
            lblUserName.text = "Login/Signup"
            lblEmail.text = ""
        }*/
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
  /*  func setProfile(){
        let fname   = AppDataManager.shared.loginData?.result?.fname ?? ""
        let lname   = AppDataManager.shared.loginData?.result?.lname ?? ""
        let img     = AppDataManager.shared.loginData?.result?.image ?? ""
        let urlStr  = URL_BASE_IMAGE_FETCH + img
        let imgURL  = URL(string: urlImage(strUrl: urlStr))
        imgProfile.sd_setImage(with: imgURL, completed: nil)
        lblUserName.text = fname  + " " + lname
        lblEmail.text = AppDataManager.shared.loginData?.result?.email
    }*/
    //-------------------------------------------------
    //MARK:- Button Action
    //-------------------------------------------------
    @IBAction func actionMyProfile(_ sender: Any) {
      
      //  dismiss(animated: true) {
         //   self.delegate?.onDissmis!()
            let vc = ProfileVC.instance(storyBoard: .Leftmenu) as! ProfileVC
          //    vc.hidesBottomBarWhenPushed = true
              self.push(viewController: vc)
       // }
    }
    @IBAction func actionAbout(_ sender: Any) {
        let vc = AboutUsVC.instance(storyBoard: .Leftmenu) as! AboutUsVC
          self.push(viewController: vc)
    }
    
    @IBAction func actionPremiumServices(_ sender: Any) {
        let vc = DatingPlanVC.instance(storyBoard: .Leftmenu) as! DatingPlanVC
          self.push(viewController: vc)
    }
    @IBAction func actionWatchList(_ sender: UIButton) {
        let vc = WatchlistVC.instance(storyBoard: .Leftmenu) as! WatchlistVC
          self.push(viewController: vc)
        
    }
    
    @IBAction func actionProvacyPolicy(_ sender: UIButton) {
        let vc = PrivacyPolicyVC.instance(storyBoard: .Leftmenu) as! PrivacyPolicyVC
          self.push(viewController: vc)
        
    }
    
    @IBAction func actionTermConditions(_ sender: UIButton) {
        let vc = PrivacyPolicyVC.instance(storyBoard: .Leftmenu) as! PrivacyPolicyVC
          self.push(viewController: vc)
    }
    
    @IBAction func actionContact(_ sender: Any) {
        let vc = ContactVC.instance(storyBoard: .Leftmenu) as! ContactVC
          self.push(viewController: vc)
    }
    
    
  
}
