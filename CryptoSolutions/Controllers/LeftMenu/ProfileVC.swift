//
//  ProfileVC.swift
//  CryptoSolutions
//
//  Created by mac on 24/02/22.
//

import UIKit

class ProfileVC: KBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    @IBAction func actionEditProfile(_ sender: Any) {
         let vc = EditProfileVC.instance(storyBoard: .Leftmenu) as! EditProfileVC
        self.push(viewController: vc)
    }

}

