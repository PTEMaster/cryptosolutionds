//
//  ProfileVC.swift
//  CryptoSolutions
//
//  Created by mac on 24/02/22.
//

import UIKit

class PrivacyPolicyVC: KBaseViewController {
    @IBOutlet weak var lblPrivacy: UILabel!
 var iscomeFromPrivacy = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if iscomeFromPrivacy{
            lblPrivacy.text  = "Privacy Policy"
        }else{
            lblPrivacy.text  = "Terms & Conditions"
        }
    }
    

    @IBAction func actionBack(_ sender: Any) {
        pop()
    }

}

