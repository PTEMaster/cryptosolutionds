//
//  ProfileVC.swift
//  CryptoSolutions
//
//  Created by mac on 24/02/22.
//

import UIKit

class NewsVC: KBaseViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var container: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = 0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        segmentedControl.selectedSegmentIndex = 0
        self.actionSegment(self)
    }

    //1
    private lazy var Segment0Comp: NewslistingVC = {
        let vc = NewslistingVC.instance(storyBoard: .News) as! NewslistingVC
        self.add(asChildViewController: vc)
        return vc
    }()
    
    //2
    private lazy var Segment1Prod: BloglistingVC = {
        let viewController = BloglistingVC.instance(storyBoard: .News) as! BloglistingVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    //3
    private lazy var Segment2Prod: EventlistingVC = {
        let viewController = EventlistingVC.instance(storyBoard: .News) as! EventlistingVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        container.addSubview(viewController.view)
        viewController.view.frame = container.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: Segment1Prod)
            remove(asChildViewController: Segment2Prod)
            add(asChildViewController: Segment0Comp)
        } else if segmentedControl.selectedSegmentIndex == 1 {
            remove(asChildViewController: Segment0Comp)
            remove(asChildViewController: Segment2Prod)
            add(asChildViewController: Segment1Prod)
        }else {
            remove(asChildViewController: Segment0Comp)
            remove(asChildViewController: Segment1Prod)
            add(asChildViewController: Segment2Prod)
        }
    }
    
  
    
    
    @IBAction func actionSegment(_ sender: Any) {
        updateView()
    }

}





/*    func call_otp_verificationAPI(){
 var param = [String : Any]()
 param[params.kuser_id] =  appDelegate.userId
 param[params.kdevice_type] = "ios"
 param[params.kmobile_otp] = strOTP
 param[params.ktoken] = ""
 ServerManager.shared.POST(url: ApiAction.verify_otp , param: param, true,header: nil) { [weak self] (data, error) in
   guard let data = data else {
         print("data not available")
         return
     }
    guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
         return
     }
     if obj.success == ResponseApis.KSuccess {
         self?.dismiss(animated: true) {
             self?.delegate?.onButtonClick(SignupStatus: self!.SignupStatus)
         }
     } else {
         print("failure")
         presentAlert("", msgStr: obj.msg, controller: self)
     }
 }
}*/


 

