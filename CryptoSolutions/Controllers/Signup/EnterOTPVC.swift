//
//  ProfileVC.swift
//  CryptoSolutions
//
//  Created by mac on 24/02/22.
//

import UIKit
protocol MoveToForgotPassDelegate {
    func onButtonClick(SignupStatus : String)
}
class EnterOTPVC: KBaseViewController {
    
    @IBOutlet weak var txtDPOTPView: HPOTPView!
    var ISComeFromForgot = false
    var txtOTPView: HPOTPView!
    var strOTP = ""
    var delegate: MoveToForgotPassDelegate?
    var strtName = ""
    var strEmail = ""
    var strMobileNo = ""
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        txtDPOTPView.hpOTPViewDelegate = self
    }
    
    @IBAction func actionContinue(_ sender: Any) {
        let valid = validation()
        if valid.status {
            signup_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }

    }
    
    @IBAction func actionResend(_ sender: Any) {
        

    }
    
    @IBAction func actiondismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        print(txtDPOTPView.validate())
        if strOTP == "" || strOTP.count < 4{
            status = false
            msg = Validation.kEnterPassword.rawValue
        }
        return (status:status, message:msg)
        
        }
    
    }




extension EnterOTPVC: HPOTPViewDelegate{

    
    func hpOTPViewAddText(_ text: String, at position: Int) {
      //  print("addText:- " + text + " at:- \(position)" )
        strOTP = text
       // print(strOTP)
       // print(txtDPOTPView.validate())
    }
    
    func hpOTPViewRemoveText(_ text: String, at position: Int) {
      //  print("removeText:- " + text + " at:- \(position)" )
        strOTP = text
        print(strOTP)
    }
    
    func hpOTPViewChangePositionAt(_ position: Int) {
     //   print("at:-\(position)")
    }
    func hpOTPViewBecomeFirstResponder() {
        
    }
    func hpOTPViewResignFirstResponder() {
        
    }
}





extension EnterOTPVC {
    func signup_API(){
        var param = [String : Any]()
        param[params.kfull_name] = strtName
        param[params.kemail] = strEmail
        param[params.kcountry_code] = "+91"
        param[params.kphone_number] = strMobileNo
        param[params.kmobile_otp] = strOTP
        param[params.kfcm_token] = ""
        param[params.kdevice_type] = "1"
        
        
        ServerManager.shared.POST(url: ApiAction.signup , param: param, true,header: nil) { [weak self] (data, error) in
          guard let data = data else {
                print("data not available")
                return
            }
         guard  let obj = try? JSONDecoder().decode(loginModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                if let  dd  =  obj.userinfo{
                    let dataa = try! JSONEncoder().encode(dd)
                    UserDefaults.standard.setUserData(value: dataa)
                    // Mark:- set user detail in local and auto login
                    UserDefaults.standard.setLoggedIn(value: true)
                    let loginData = UserDefaults.standard.getUserData()
                    let obj = try? JSONDecoder().decode(Userinfo.self, from: loginData)
                   AppDataHelper.shared.logins = obj
                    SwitchNav.homeRootNavigation()
                }
         
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
   
}
