/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Userinfo : Codable {
	let user_id : String?
	let full_name : String?
	let email : String?
	let country_code : String?
	let phone_number : String?
	let fcm_token : String?
	let profile_image : String?
	let device_type : String?
	let expiry_date : String?
	let plan_id : String?
	let purchase_token : String?
	let notification_count : String?
	let status : String?

	enum CodingKeys: String, CodingKey {

		case user_id = "user_id"
		case full_name = "full_name"
		case email = "email"
		case country_code = "country_code"
		case phone_number = "phone_number"
		case fcm_token = "fcm_token"
		case profile_image = "profile_image"
		case device_type = "device_type"
		case expiry_date = "expiry_date"
		case plan_id = "plan_id"
		case purchase_token = "purchase_token"
		case notification_count = "notification_count"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
		phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
		fcm_token = try values.decodeIfPresent(String.self, forKey: .fcm_token)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
		expiry_date = try values.decodeIfPresent(String.self, forKey: .expiry_date)
		plan_id = try values.decodeIfPresent(String.self, forKey: .plan_id)
		purchase_token = try values.decodeIfPresent(String.self, forKey: .purchase_token)
		notification_count = try values.decodeIfPresent(String.self, forKey: .notification_count)
		status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}