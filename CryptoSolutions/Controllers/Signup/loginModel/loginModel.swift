

import Foundation
struct loginModel : Codable {
	let success : String?
	let message : String?
	let userinfo : Userinfo?

	enum CodingKeys: String, CodingKey {

		case success = "success"
		case message = "message"
		case userinfo = "userinfo"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		success = try values.decodeIfPresent(String.self, forKey: .success)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		userinfo = try values.decodeIfPresent(Userinfo.self, forKey: .userinfo)
	}

}
