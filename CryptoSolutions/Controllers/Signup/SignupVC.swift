//
//  ProfileVC.swift
//  CryptoSolutions
//
//  Created by mac on 24/02/22.
//

import UIKit

class SignupVC: KBaseViewController {

    @IBOutlet weak var lblTermsCondition: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfMobileNo: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedWithTextColor: NSAttributedString = "By creating an account, you agree to our Terms & Conditions".attributedStringWithColor(["Terms & Conditions"], color: appcolor!)

        lblTermsCondition.attributedText = attributedWithTextColor
        
    }
    
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        if tfName.text == "" {
            status = false
            msg = Validation.kEnterName.rawValue
        }
        else if  tfEmail.text == ""{
            status = false
            msg = Validation.kEnterEmail.rawValue
        }
        else if self.isValidEmail(candidate: tfEmail.text!) {
            status = false
            msg = Validation.kEnterValidEmail.rawValue
        }
        else if   tfMobileNo.text == ""{
            status = false
            msg = Validation.kEnterMobileNumber.rawValue
        }
        return (status:status, message:msg)
    }
    
    
    
    
    @IBAction func actionGetStart(_ sender: Any) {
        let valid = validation()
        if valid.status {
            otp_verificationSignup_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    @IBAction func actionCheckUncheck(_ sender: Any) {
    }
    
}

extension SignupVC {
    func otp_verificationSignup_API(){
        var param = [String : Any]()
        param[params.kemail] = tfEmail.text
        param[params.kcountry_code] = "+91"
        param[params.kphone_number] = tfMobileNo.text
        
        ServerManager.shared.POST(url: ApiAction.otp_verificationSignup , param: param, true,header: nil) { [weak self] (data, error) in
          guard let data = data else {
                print("data not available")
                return
            }
        guard  let obj = try? JSONDecoder().decode(success_model.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
         let vc = EnterOTPVC.instance(storyBoard: .Signup) as! EnterOTPVC
                vc.strtName = (self?.tfName.text)!
                vc.strEmail = (self?.tfEmail.text!)!
                vc.strMobileNo = self?.tfMobileNo.text! ?? ""
                self?.push(viewController: vc)
         
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
   
}


struct success_model : Codable {
    let success : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}


extension String {
    func attributedStringWithColor(_ strings: [String], color: UIColor, characterSpacing: UInt? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in strings {
            let range = (self as NSString).range(of: string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }

        guard let characterSpacing = characterSpacing else {return attributedString}

        attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))

        return attributedString
    }
}
