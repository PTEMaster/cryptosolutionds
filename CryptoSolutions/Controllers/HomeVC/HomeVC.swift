//
//  HomeVC.swift
//  CryptoSolutions
//
//  Created by mac on 23/02/22.
//

import UIKit
import SDWebImage
import SideMenu
import Starscream

class HomeVC: KBaseViewController {
    
    @IBOutlet weak var tblMarket: UITableView!
    
    var sidemenuVC    : SideMenuNavigationController!
    var socket: WebSocket!
    var isConnected = false
    let server = WebSocketServer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SwitchNav.sideMenuConfiger(&sidemenuVC)
        sidemenuVC.settings = SwitchNav.makeSettings(SwitchNav())()
        setTabBarItems()
        var request = URLRequest(url: URL(string: "wss://stream.binance.com:9443/ws/bnbusdt@trade/etheur@trade")!) //https://localhost:8080
        request.timeoutInterval = 5
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }
    override func viewDidAppear(_ animated: Bool) {
      //  self.tabBarController!.tabBar.backgroundColor = .white
    }
    
    @IBAction func menuBTN_Action(_ sender: Any) {
        self.present(sidemenuVC, animated: true) {
            
        }
    }

    @IBAction func actionNotifiacation(_ sender: Any) {
       let vc = NotificationVC.instance(storyBoard: .Home) as! NotificationVC
        self.push(viewController: vc)
    }
    
    // MARK: Write Text Action
    
   /* @IBAction func writeText(_ sender: UIBarButtonItem) {
        socket.write(string: "hello there!")
    }
    
    // MARK: Disconnect Action
    
    @IBAction func disconnect(_ sender: UIBarButtonItem) {
        if isConnected {
            sender.title = "Connect"
            socket.disconnect()
        } else {
            sender.title = "Disconnect"
            socket.connect()
        }
    }*/
    
}

extension HomeVC: UITableViewDelegate , UITableViewDataSource  {
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarketCell", for: indexPath) as! MarketCell
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let data = obj![indexPath.row]
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}




extension HomeVC : WebSocketDelegate{
   
    
    // MARK: - WebSocketDelegate
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            isConnected = true
            print("websocket is connected: \(headers)")
        case .disconnected(let reason, let code):
            isConnected = false
            print("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            print("Received text: \(string)")
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
        case .error(let error):
            isConnected = false
            handleError(error)
        }
    }
    
    func handleError(_ error: Error?) {
        if let e = error as? WSError {
            print("websocket encountered an error: \(e.message)")
        } else if let e = error {
            print("websocket encountered an error: \(e.localizedDescription)")
        } else {
            print("websocket encountered an error")
        }
    }
    
}



class MarketCell: UITableViewCell {
}
