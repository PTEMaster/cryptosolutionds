//
//  PageConstent.swift
//  IRatePro
//
//  Created by mac on 18/02/21.
//

import Foundation

enum Page {
    case CompanyProductVC
    case BestReviewVC
    case AddReviewOnProductVC
    case HomeVC
    case PaymentVC
    case AllCompaniesVC
    case CompanyNameVC
    case Segment0Company
    case Segment1Product
    case none
}

