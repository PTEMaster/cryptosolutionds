//
//  Constent.swift
//  Auction
//
//  Created by mac on 10/12/20.
//

import Foundation
import  UIKit
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let kAppName = "Crypto Solutions"
let kisLogin = "isLogin"
var myLat = "0.0"
var myLong = "0.0"


let commonUrl = "https://ctinfotech.com/CT01/cryptoSolution/Api/"

let baseUrl  =  "\(commonUrl)"

let PostImagePath = "\(commonUrl)assets/post/"

let ProfileImagePath  = "\(commonUrl)assets/userfile/profile/"

let CategoryImagePath = "\(commonUrl)assets/category/"

let  VideoImagePath =  "\(commonUrl)assets/reels/"

let  ConcertImagePath =  "\(commonUrl)assets/concerts/"

let  goalImagePath  = "\(commonUrl)assets/images/"

let  songsPath  = "\(commonUrl)assets/songs/"



let  termsCondition = "https://ctinfotech.com/CTCC/artist_army/home/terms"

let  privacypolicy = "https://ctinfotech.com/CTCC/artist_army/home/privacy"




//struct appColor {
let bgcolor =  UIColor(named: "bgcolor")
let Pink = UIColor(named: "Pink")
let White = UIColor(named: "White")
let whiteBase = UIColor(named: "whiteBase")
let Black = UIColor(named: "Black")
let lightPink = UIColor(named: "lightPink")
let darkBlue = UIColor(named: "DarkBlue")

let yellow = UIColor(named: "yellow")
let yellow_light = UIColor(named: "yellow_light")

let green_light = UIColor(named: "green_light")
let green = UIColor(named: "green")

let red = UIColor(named: "Red")
let appcolor = UIColor(named: "appcolor")






let  lightPurple = UIColor(named: "lightPurple")
/*
let gradgray = UIColor(named: "grey")
let gradCoral = UIColor(named: "Coral")
let green = UIColor(named: "green")

let darkgreen = UIColor(named: "darkgreen")*/
//}


//struct appColor {
//    let green = UIColor(named: "green")
//}



struct ApiAction {
    static let otp_verificationSignup = "otp_verificationSignup"
    static let signup = "signup"

    
}



//MARK:- Validation string
enum Validation: String {
    case kEnterMobileNumber = "Please enter mobile number"
    case kEnterName = "Please enter name"
    case kDOB = "Please select date of birth"
    
    case kaboutyourself = "Please enter about yourself"
    case kidealdate = "Please enter about your ideal date"
    
    ////////////////////////////////////////////////////////////////////
   
    case kEnterEmail = "Please Enter Your Email"
    case kEnterValidEmail = "Please Enter Valid Email Address"
    case kEnterValidNumber = "Please Enter Valid Mobile Number"
    case kEnterCompanyName = "Please Enter Company Name"
    case kEnterEINNumber = "Please Enter EIN Number"
    case kEnterBusinessType = "Please Select Business Type"
    case kEnterWebUrl = "Please Enter Website Url"
    case kSelectCategory = "Please Select Category"
    case kEnterCompanyDis = "Please Enter Company Description"
    case kEnterAddress = "Please Enter Your Address"
    case kEnterPassword = "Please Enter Your Password"
    case kEnterConfirmPassword = "Please Enter Confirm Password"
    case kEnterValidPassword = "Please Enter Valid Password"
    case kPassowrdNotMatched = "New Password & Confirm Password Not Matched"
    case kEnterNewPassword = "Please Enter New Password"
    
    case kProductName = "Please Enter Product Name"
    case kProductDetail = "Please Enter Product Detail"
    case kProductDiscription = "Please Enter Product Description"
    case kEnterTitle = "Please Enter Title"
    case kEnterDetail = "Please Enter Detail"
    case kDiscription = "Please Enter Description"
    case kUserType = "Please Select UserType"
    case kCompanyImg = "Please Select Company image"
    case kCountryName = "Please Enter Country name"
    case kCityName = "Please Enter City name"
    case kStateName = "Please Enter State name"
    case kPostalCode = "Please Enter PostalCode"
    case kprivacy = "Please check privacy & policy"
    case kdate = "Please select date"
    case ktime = "Please select time"
    case kvenue = "Please enter  concert venue"
    case kdescription = "Please enter concert description"
    case kdonateAmount = "Please enter donate amount"
    case kdonateValidAmount = "Please enter valid donate amount"
    case kGoalName = "Please enter goal name"
    case kCerditsNedded = "Please enter amount of credits needed"
    case kImageUpload  = "Please upload image"
    case kreview  = "Please give review to us"
    case kreason = "Please provide reason to us"
}

//MARK:- Button titile string
enum ButtonTitle: String {
    case kOk = "OK"
    case kCancel = "CANCEL"
    case kYes = "YES"
    case kNo = "NO"
}


struct Message {
    static let msgSorry            = "Sorry something went wrong."
    static let msgTimeOut          = "Request timed out."
    static let msgCheckConnection  = "Please check your connection and try again."
    static let msgConnectionLost   = "Network connection lost."
    static let Key_Alert           = ""
    static let couldNotConnect     = "Could not able to connect with server. Please try again."
    let networkAlertMessage   = "Please Check Internet Connection"
}


struct params {
    static let kfull_name  = "full_name"
    static let kemail  = "email"
    static let kcountry_code  = "country_code"
    static let kphone_number  = "phone_number"
    static let kmobile_otp  = "mobile_otp"
    static let kfcm_token  = "fcm_token"
    static let kdevice_type  = "device_type"

    
    
    
}



extension HomeVC{
    func setTabBarItems(){
        let tabBar = self.tabBarController!.tabBar
        
        let myTabBarItem1 = (tabBar.items?[0])! as UITabBarItem
        myTabBarItem1.image = UIImage(named: "home")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.selectedImage = UIImage(named: "home_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.title = ""
       myTabBarItem1.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        let myTabBarItem2 = (tabBar.items?[1])! as UITabBarItem
        myTabBarItem2.image = UIImage(named: "notification")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.selectedImage = UIImage(named: "notification_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.title = ""
        myTabBarItem2.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        
        let myTabBarItem3 = (tabBar.items?[2])! as UITabBarItem
        myTabBarItem3.image = UIImage(named: "video")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.selectedImage = UIImage(named: "video_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.title = ""
        myTabBarItem3.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -6, right: 0)
        
    }
}




extension UIDevice {
    var hasNotch: Bool {
        if #available(iOS 11.0, *) {
           return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.bottom ?? 0 > 0
        }
        return false
   }
}



func getCountryCallingCode(countryRegionCode:String)->String{
    let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263" , "":""]
    let countryDialingCode = prefixCodes[countryRegionCode]
    return countryDialingCode!
}


class tabBar: UITabBarController{
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 15.0, *) {
               let appearance = UITabBarAppearance()
               appearance.configureWithOpaqueBackground()
            appearance.backgroundColor =  bgcolor
               self.tabBar.standardAppearance = appearance
               self.tabBar.scrollEdgeAppearance = appearance
        }else{
                    UITabBar.appearance().shadowImage     = UIImage()
                    UITabBar.appearance().clipsToBounds   = true
            UITabBar.appearance().barTintColor =  bgcolor
            self.tabBar.isTranslucent = false
        }
        
        
      //  self.tabBar.tintColor = UIColor.white // tab bar icon tint color
      
        // tab bar background color
    }
}
