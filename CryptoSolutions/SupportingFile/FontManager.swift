//
//  FontManager.swift
//  PTEMaster
//
//  Created by mac on 17/08/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation
import UIKit

let NunitoLight = "Montserrat-Light"
let FontRegular = "Montserrat-Regular"
let FontSemiBold = "Montserrat-SemiBold"
let FontBold = "Montserrat-Bold"
let ExtraBold = "Montserrat-ExtraBold"

let FontLatoRegular = "Lato-Regular"
let FontLatoBold = "Lato-Bold"





enum Fonts : Int {
    case NunitoLight = 0
    case regular = 1
    case semibold = 2
    case bold = 3
    case extrabold = 4
    
    case latoregular = 11
    case latobold = 22
    
    public func font(WithSize size : CGFloat) -> UIFont {
        switch self {
        case .regular:
           return UIFont.regularFont(size: size)
        case.semibold:
        return UIFont.semiBoldFont(size: size)
        case .bold:
            return UIFont.boldFont(size: size)
        case .NunitoLight:
            return UIFont.sunitoLight(size: size)
        case .extrabold:
            return UIFont.extraBoldFont(size: size)
        case .latoregular:
            return UIFont.regularLatoFont(size: size)
        case .latobold:
            return UIFont.boldLatoFont(size: size)
        }
    }
}

extension UIFont {
   
    static func sunitoLight( size:CGFloat ) -> UIFont{
        return  UIFont(name: NunitoLight , size: size)!
    }
    
    static func regularFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontRegular , size: size)!
    }
    
    static func boldFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontBold , size: size)!
    }
    
    
    static func semiBoldFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontSemiBold , size: size)!
    }
    
    static func extraBoldFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: ExtraBold , size: size)!
    }
    
    
    static func regularLatoFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontLatoRegular , size: size)!
    }
    
    static func boldLatoFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontLatoBold , size: size)!
    }
    
}

//--------------

extension UILabel {
    @IBInspectable  var CustomFont: Int  {
        get {
            return self.CustomFont
        }
        set {
            self.font = Fonts.init(rawValue: newValue)?.font(WithSize: self.font.pointSize)
        }
    }
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
        
        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
}

extension UITextField {
    @IBInspectable  var CustomFont: Int  {
            get {
                return self.CustomFont
            }
        set {
            self.font = Fonts.init(rawValue: newValue)?.font(WithSize: self.font?.pointSize ?? 0)
        }
    }
}
extension UITextView {
    @IBInspectable  var CustomFont: Int  {
            get {
                return self.CustomFont
            }
        set {
            self.font = Fonts.init(rawValue: newValue)?.font(WithSize: self.font?.pointSize ?? 0)
        }
    }
}


extension UIButton {
    @IBInspectable  var CustomFont: Int  {
            get {
                return self.CustomFont
            }
        set {
            self.titleLabel?.font = Fonts.init(rawValue:  newValue)?.font(WithSize: self.titleLabel?.font?.pointSize ?? 1)
        }
    }
}
