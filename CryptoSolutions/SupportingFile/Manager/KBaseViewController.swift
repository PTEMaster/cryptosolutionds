//

//
//  Created by mac on 29/06/21.
//  Copyright © 2020 Cti. All rights reserved.
//

import UIKit

class KBaseViewController: UIViewController {
    let offlineViewC = OfflineViewController(nibName: "OfflineViewController", bundle: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
       
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()
    }
    func updateUserInterface() {
       /* switch Network.reachability.status {
        case .unreachable:
            view.backgroundColor = .red
        case .wwan:
            view.backgroundColor = .yellow
        case .wifi:
            view.backgroundColor = .green
        }*/
       // print("Reachability Summary")
       // print("Status:", Network.reachability.status)
      // print("HostName:", Network.reachability.hostname ?? "nil")
     //   print("Reachable:", Network.reachability.isReachable)
        
      // print("Wifi:", Network.reachability.isReachableViaWiFi)
        
       /* if Network.reachability.isReachable{
            dismissUnreachableWindow()
        }else{
            presentUnreachableWindow()
        }*/
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    func presentUnreachableWindow() {
        
        if #available(iOS 13.0, *) {
            if var topController = UIApplication.shared.keyWindow?.rootViewController  {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }//offlineViewVC
                if topController is OfflineViewController {
                }else{
                   // self.offlineViewC.modalPresentationStyle = .overFullScreen
                   // topController.present(offlineViewC, animated: true, completion: nil)
                }
            }
        }
    }
    
    func dismissUnreachableWindow() {
        self.offlineViewC.dismiss(animated: true, completion: nil)
    }
    

     func showAlertWithCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: (() -> Void)? = nil) {
    
         let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
         
         if isWithCancel {
             alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
         }
         
         alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
             handler?()
         }))
     
         self.present(alertController, animated: true, completion: nil)
     }
     
    
    func format(phoneNumber: String, shouldRemoveLastDigit: Bool = false) -> String {
        guard !phoneNumber.isEmpty else { return "" }
        guard let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive) else { return "" }
        let r = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: .init(rawValue: 0), range: r, withTemplate: "")
        if number.count > 10 {
            let tenthDigitIndex = number.index(number.startIndex, offsetBy: 10)
            number = String(number[number.startIndex..<tenthDigitIndex])
        }

        if shouldRemoveLastDigit {
            let end = number.index(number.startIndex, offsetBy: number.count-1)
            number = String(number[number.startIndex..<end])
        }

        if number.count < 7 {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d+)", with: "($1) $2", options: .regularExpression, range: range)

        } else {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: range)
        }

        return number
    }
    
    // Mobile number Valudation
    func isValidPhone(phone: String) -> Bool {
        let phoneRegex = "^[0-9]{6,14}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
    
    
    func actionLogout() {
        let alertController = UIAlertController(title: nil, message: "Are you sure do you want to logout?", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (actionlogout) in
            UserDefaults.standard.removeAllUserdefault()
            SwitchNav.loginRootNavigation()
            
             self.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Email Validation
    func isValidEmail(candidate: String) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        if valid {
            valid = !candidate.contains("..")
        }
        return !valid
    }
    
    //MARK:- Methods and funcation
    func isAnimationPop(isAdd:Bool, toView:UIView) {
        if isAdd {
             toView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: toView.frame.size.height)
            toView.frame.origin.y = self.view.frame.size.height
            self.view.addSubview(toView)
            UIView.animate(withDuration: 0.5) {
                toView.frame.origin.y = self.view.frame.size.height - toView.frame.size.height
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: .nan, options: .curveEaseInOut, animations: {
               toView.frame.origin.y = self.view.frame.size.height + self.view.frame.size.height
            }) { (status) in
            toView.removeFromSuperview()
            }
        }
    }
    

}
