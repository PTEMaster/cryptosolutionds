
import Foundation
import UIKit
import SideMenu

class SwitchNav {
    static func homeRootNavigation(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        let vc = UIStoryboard(name: StoryBoard.Signup.rawValue, bundle: nil).instantiateViewController(withIdentifier: "home") as! UINavigationController
        if #available(iOS 13.0, *){
            if let scene = UIApplication.shared.connectedScenes.first{
                guard let windowScene = (scene as? UIWindowScene) else { return }
                //print(">>> windowScene: \(windowScene)")
                let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                window.windowScene = windowScene //Make sure to do this
                window.rootViewController = vc
                window.makeKeyAndVisible()
                appDelegate.window = window
            }
        } else {
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    static func loginRootNavigation(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let vc = UIStoryboard(name: StoryBoard.Signup.rawValue, bundle: nil).instantiateViewController(withIdentifier: "signup") as! UINavigationController
        if #available(iOS 13.0, *){
            if let scene = UIApplication.shared.connectedScenes.first{
                guard let windowScene = (scene as? UIWindowScene) else { return }
                //print(">>> windowScene: \(windowScene)")
                let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                window.windowScene = windowScene //Make sure to do this
                window.rootViewController = vc
                window.makeKeyAndVisible()
                appDelegate.window = window
            }
        } else {
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    class func sideMenuConfiger(_ menuNavigation: inout SideMenuNavigationController?) {
        let vc = SideVC.instance(storyBoard: .Signup) as! SideVC
      //  vc.delegate = delegate
        menuNavigation = SideMenuNavigationController(rootViewController: vc)
        menuNavigation!.leftSide = true
        menuNavigation!.statusBarEndAlpha =  0
        menuNavigation?.menuWidth = 120
        menuNavigation!.isNavigationBarHidden = true
        menuNavigation!.menuWidth = UIScreen.main.bounds.width * 0.7
        menuNavigation!.blurEffectStyle = .light
       // menuNavigation!.presentationStyle = .menuSlideIn
    }
    
    func makeSettings() -> SideMenuSettings {
        let presentationStyle = selectedPresentationStyle()
        presentationStyle.presentingEndAlpha = CGFloat(0.3)
        var settings = SideMenuSettings()
        settings.presentationStyle = presentationStyle
        settings.menuWidth = UIScreen.main.bounds.width * 0.8
        return settings
    }
    func selectedPresentationStyle() -> SideMenuPresentationStyle {
        let modes: [SideMenuPresentationStyle] = [.menuSlideIn, .viewSlideOut, .viewSlideOutMenuIn, .menuDissolveIn]
        return modes[0]
    }
    
    
}






