//
//  ServerManger.swift
//  Eatsy
//
//  Created by Apple on 01/10/19.
//  Copyright © 2019 Rajesh Shinde. All rights reserved.
//

import UIKit
import Alamofire

//typealias CompletionHandler = (_ resposnObj: AFDataResponse<Any>) -> Void
typealias JSONHandler = (_ responseJSON: [String: Any]) -> Void
typealias completionHandlerWithError = (_ responseJson: Data?, _ error: Error?) -> Void

class ServerManager {
    static let shared = ServerManager()
    
    private  init() {
        
    }
    //MARK:- Post Method with header with body
  
    func POST(url: String, param: Parameters,_ ShowLoader : Bool? = true ,_ Showlog : Bool? = true , header: HTTPHeaders?, responseBlock: @escaping completionHandlerWithError){

        if !ReachabilityTraydi.isConnectedToNetwork() {

            DispatchQueue.main.async(execute: {
              
                presentAlert("", msgStr: Message.msgCheckConnection, controller: self)
            })
            return
        }
        if  ShowLoader!{
            Loader.showLoader()
        }
      
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, to: baseUrl +  url, method: .post , headers: nil).responseJSON { (response) in
            
            switch response.result {
            
            case .success(let upload):
                if  Showlog!{
                print("\n\n------------------------------------")
                print("API:- \(baseUrl +  url)")
                print("Param:- \(param)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                }
                responseBlock(response.data, nil )
               
            case .failure(let error):
                print("\n\n---------------Error-------------")
                print("API:- \(baseUrl +  url)")
                print("Param:- \(param)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                
                presentAlert("", msgStr: error.localizedDescription, controller: self)
                responseBlock(nil, error )
            }
            
            if  ShowLoader!{
            Loader.hideLoader()
            }
        }
    }

    //MARK:- Get Method with header with body
     func GET(url: String, param: Parameters,_ ShowLoader : Bool? = true ,_ Showlog : Bool? = true  , header: HTTPHeaders?, responseBlock:@escaping completionHandlerWithError){
        
        if !ReachabilityTraydi.isConnectedToNetwork() {

           // let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
            DispatchQueue.main.async(execute: {
                
                presentAlert("", msgStr: Message.msgCheckConnection, controller: self)
            })
            
            return
          
        }
        
         if  ShowLoader!{
             Loader.showLoader()
         }
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, to:baseUrl + url, method: .get , headers: header).responseJSON { (response) in
   
            if  ShowLoader!{
            Loader.hideLoader()
            }
            switch response.result {
            
            case .success(let upload):
                if  Showlog!{
                print("\n\n------------------------------------")
                print("API:- \( baseUrl + url)")
                print("Param:- \(param)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                }
                responseBlock(response.data, nil)
            case .failure(let error):
                if  Showlog!{
                print("\n\n---------------Error-------------")
                print("API:- \( baseUrl + url)")
                print("Param:- \(param)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                }
                responseBlock(nil, error )
            }
        }
        
    }
    //MARK:- POST With Image
    func POSTWithImage(url: String, param: Parameters,imgParam: String?  = "image" , imageView: UIImageView, responseBlock:@escaping completionHandlerWithError){
        let image = imageView.image?.jpegData(compressionQuality: 0.3)
        if !ReachabilityTraydi.isConnectedToNetwork() {
            DispatchQueue.main.async(execute: {
                presentAlert("", msgStr: Message.msgCheckConnection, controller: self)
            })
            return
        }
        Loader.showLoader()
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if  image?.count ?? 0 > 0{
                multipartFormData.append(image!, withName: imgParam!,fileName: "file.jpg", mimeType: "image/jpg")
                }
        }, to:  baseUrl + url, method: .post , headers: nil).responseJSON { (response) in
            
            Loader.hideLoader()
            switch response.result {
            
            case .success(let upload):
                print("\n\n------------------------------------")
                print("API:- \(  baseUrl + url)")
                print("Param:- \(param)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                responseBlock(response.data, nil)
            case .failure(let error):
                print("\n\n---------------Error-------------")
                print("API:- \(  baseUrl + url)")
                print("Param:- \(param)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                responseBlock(nil, error)
            }
        }
    }
    
    
    func performMultipleImageUpload(url: String , _ parameter:[String:Any], _ imageParameter:[String:Data], responseBlock:@escaping completionHandlerWithError) -> Void {
       
        if !ReachabilityTraydi.isConnectedToNetwork() {
            DispatchQueue.main.async(execute: {
                presentAlert("", msgStr: Message.msgCheckConnection, controller: self)
            })
        }
            Loader.showLoader()
                 AF.upload(multipartFormData: { (multipartFormData) in
                     for (key, value) in parameter {
                         multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                     }
                   
                   for (key, value) in imageParameter {
                       let imageData = value as Data
                       multipartFormData.append(imageData, withName: "\(key)" ,fileName: UUID().uuidString + ".jpg", mimeType: "image/jpg")
                     print("Image Upload:-==============================")
                   }
                   
                     
                 }, to:  baseUrl +  url, usingThreshold: UInt64.init(), method: .post, headers: nil).responseJSON { (result) in
                    print("Response:-\(result.result)")
                    print("Response:-\(result)")
                    Loader.hideLoader()
                     switch result.result {
                     
                     case .success(let upload):
                         print("\n\n------------------------------------")
                         print("API:- \( baseUrl + url)")
                         print("Param:- \(parameter)")
                         print("\nresponse:- \(upload)")
                         print("--------------------------------------\n\n")
                         responseBlock(result.data, nil)
                         
                     case .failure(let error):
                         print("\n\n---------------Error-------------")
                         print("API:- \(url)")
                         print("Param:- \(parameter)")
                         print("\nError:- \(error)")
                         print("--------------------------------------\n\n")
                         responseBlock(nil, error)
                     }
                 }
           }
    

    
    func MultipleImageUpload(url: String ,params:[String:Any], imageParams:[UIImage], isVideoUpload : Bool? = false , responseBlock:@escaping completionHandlerWithError) {
        Loader.showLoader("Loading....")
        if !ReachabilityTraydi.isConnectedToNetwork() {
            DispatchQueue.main.async(execute: {
                presentAlert("", msgStr: Message.msgCheckConnection, controller: self)
            })
        }
        print("postUrl-=-\(  baseUrl + url)")
        print("params-=-\(params)")
        print("imageParams=\(imageParams)=")
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for images in imageParams {
                if isVideoUpload! {
                    
                }else{
            print("Upload photo-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
                    let thumb1 = images.resized(withPercentage: 0.5)
                        let thumb2 = thumb1?.resized(toWidth: 50.0)
                        guard let data = thumb1?.pngData() else {
                            return
                        }
                        let imageName = UUID().uuidString + ".png"
                        multipartFormData.append(data, withName: "images[]" ,fileName: imageName , mimeType: "image/jpg")
                   
                }
            }
          
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        },to:    baseUrl + url , usingThreshold: UInt64.init(), method: .post, headers: nil).responseJSON { (result) in
            Loader.hideLoader()
            switch result.result {
           
            case .success(let upload):
                print("\n\n------------------------------------")
                print("API:- \( baseUrl + url)")
                print("Param:- \(params)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                responseBlock(result.data, nil)
                
            case .failure(let error):
                print("\n\n---------------Error-------------")
                print("API:- \(url)")
                print("Param:- \(params)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                responseBlock(nil, error)
            }
        }
        
    }
    
    
    func VideoUpload(url: String ,params:[String:Any], imageParams:[[String:Any]], isVideoUpload : Bool? = false , responseBlock:@escaping completionHandlerWithError) {
        Loader.showLoader("Loading....")
        if !ReachabilityTraydi.isConnectedToNetwork() {
            DispatchQueue.main.async(execute: {
                presentAlert("", msgStr: Message.msgCheckConnection, controller: self)
            })
        }
        print("postUrl-=-\(  baseUrl + url)")
        print("params-=-\(params)")
        print("imageParams=\(imageParams)=")
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for images in imageParams {
                if isVideoUpload! {
                    print("Upload video-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
                     for (key, value) in images {
                         let videoUrl = value as! URL
                        do {
                            let data = try Data(contentsOf: videoUrl, options: .mappedIfSafe)
                            print(data)
                            print("File size before compression: \(Double(data.count / 1048576)) mb")
                            //  here you can see data bytes of selected video, this data object is upload to server by multipartFormData upload
                            let imageName = UUID().uuidString + ".mp4"
                            print(imageName)
                            multipartFormData.append(data, withName: key ,fileName: imageName , mimeType: "video/mp4")
                        } catch  {
                            return
                        }
                    }
                }else{
                      print("Upload photo-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
                    for (key, value) in images {
                        let image = value as? UIImage
                        let thumb1 = image?.resized(withPercentage: 0.5)
                        guard let data = thumb1?.pngData() else {
                            return
                        }
                        let imageName = UUID().uuidString + ".png"
                        multipartFormData.append(data, withName: key ,fileName: imageName , mimeType: "image/jpg")
                    }
                }
            }
          
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        },to:    baseUrl + url , usingThreshold: UInt64.init(), method: .post, headers: nil).responseJSON { (result) in
            Loader.hideLoader()
            switch result.result {
           
            case .success(let upload):
                print("\n\n------------------------------------")
                print("API:- \( baseUrl + url)")
                print("Param:- \(params)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                responseBlock(result.data, nil)
                
            case .failure(let error):
                print("\n\n---------------Error-------------")
                print("API:- \(url)")
                print("Param:- \(params)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                responseBlock(nil, error)
            }
        }
        
    }
    
    
}


struct ResponseApis {
    static let KSuccess = "1"
    static let kError = "0"
}




extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    
    /*
     let image = UIImage(data: try! Data(contentsOf: URL(string:"http://i.stack.imgur.com/Xs4RX.jpg")!))!

     let thumb1 = image.resized(withPercentage: 0.1)
     let thumb2 = image.resized(toWidth: 72.0)*/
}
