//
//  RSGradientView.swift
//  Music
//
//  Created by mac on 08/12/20.
//

import UIKit


@IBDesignable
class RSGradientbtn: UIButton {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.black
    @IBInspectable var startPoint : CGPoint = CGPoint(x: 0.0, y: 1.0)
    @IBInspectable var endPoint: CGPoint = CGPoint(x: 1.0, y: 0.0)
 
    public let buttongradient: CAGradientLayer = CAGradientLayer()
   
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
        (layer as! CAGradientLayer).startPoint = startPoint
        (layer as! CAGradientLayer).endPoint = endPoint
        //self.updateGradient()
    }
    
}

@IBDesignable
class RSGradientLabel: UILabel {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.black
    @IBInspectable var startPoint : CGPoint = CGPoint(x: 0.0, y: 1.0)
    @IBInspectable var endPoint: CGPoint = CGPoint(x: 1.0, y: 0.0)
 
    public let buttongradient: CAGradientLayer = CAGradientLayer()
   
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
        (layer as! CAGradientLayer).startPoint = startPoint
        (layer as! CAGradientLayer).endPoint = endPoint
       
        //self.updateGradient()
    }
    
    
    
}
//----------------- hp

class GradientButton: UIButton {
    
    public let buttongradient: CAGradientLayer = CAGradientLayer()
    
    override var isSelected: Bool { // or isHighlighted?
        didSet {
            updateGradientColors()
        }
    }
    
    func updateGradientColors() {
        let colors: [UIColor]
        if isSelected {
            
            colors = [White!, lightPurple!]
        } else {
            colors = [White!, lightPurple!]
        }
        if  isSelected{
            buttongradient.startPoint = CGPoint(x: 3.0, y: 0.0)
            buttongradient.endPoint = CGPoint(x: 0.0, y: 1.0)
            self.layer.insertSublayer(buttongradient, at: 0)
        }else{
            buttongradient.startPoint = CGPoint(x: 3.0, y: 0.0)
            buttongradient.endPoint = CGPoint(x: 0.0, y: 2.0)
            self.layer.insertSublayer(buttongradient, at: 0)
        }
        
        buttongradient.colors = colors.map { $0.cgColor }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupGradient()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.updateGradient()
    }
    
    func setupGradient() {
     
       
        
        updateGradientColors()
      
    }
    
    func updateGradient() {
        buttongradient.frame = self.bounds
        //buttongradient.cornerRadius = buttongradient.frame.height / 2
    }
}




//=================

