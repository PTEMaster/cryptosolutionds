//
//  Loader.swift
//  DemoWebervice
//
//  Created by MacMini on 03/01/20.
//  Copyright © 2020 Codiant. All rights reserved.
//

import UIKit
import RSLoadingView

class Loader: UIView {

    static var loaderView : Loader?
    
    @IBOutlet weak  var imgView : UIImageView?
    @IBOutlet weak var viewBg : UIView?
    @IBOutlet weak var label: UILabel?

    
    override func awakeFromNib() {
       // imgView?.image = imgView?.image!.withRenderingMode(.alwaysTemplate)
       // imgView?.tintColor = UIColor.blue
    }
    
    class  func showLoader(_ title: String? = nil) {
        DispatchQueue.main.async {
            
            let loadingView = RSLoadingView(effectType: RSLoadingView.Effect.twins)
           // let loadingView = RSLoadingView()
            loadingView.shouldTapToDismiss = false
            loadingView.mainColor = .blue
            loadingView.speedFactor = 1.3
            loadingView.dimBackgroundColor = UIColor.white.withAlphaComponent(0.4)
          //  loadingView.sizeFactor = 1.2
            /*speedFactor: mainColor: colorVariation: sizeFactor: spreadingFactor: lifeSpan*/
           
          // loadingView.spreadingFactor = 1.5
            //loadingView.lifeSpanFactor = 1.5
            loadingView.showOnKeyWindow()

                            //UIApplication.shared.keyWindow?.addSubview(loaderView!)
            
            
             
       /* if  loaderView != nil {
            hideLoader()
        }
        loaderView = UINib(nibName: "Loader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? Loader
        loaderView?.tag = 2000
        UIApplication.shared.keyWindow?.addSubview(loaderView!)
        loaderView!.frame = UIScreen.main.bounds
        loaderView?.label?.text = title

        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            loaderView?.viewBg!.alpha = 0.5
        }) { (finished) -> Void in
        }
        let rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation.fromValue = 0
        rotation.toValue = 2 * Double.pi
        rotation.duration = 1.1
        rotation.repeatCount = Float.infinity
        loaderView?.imgView!.layer.add(rotation, forKey: "Spin")
         loaderView?.imgView?.tintColor = UIColor(named: "tabBartintColor")*/
        }
    }
    
    class func hideLoader() {
        let loadingView = RSLoadingView()
      
      
   loadingView.hideOnKeyWindow()
        
     /*   UIView.animate(withDuration: 0.0, animations: {
            loaderView?.viewBg!.alpha=0.1
        }) { (completion) in
            if  loaderView != nil {
                for subview : UIView in ( UIApplication.shared.keyWindow?.subviews)! {
                    if subview.tag == 2000 {
                        subview.removeFromSuperview()
                        loaderView = nil
                    }
                }
            }
        }*/
    }
}
