//
//  StoryBoardManager.swift
//  Auction
//
//  Created by mac on 10/12/20.
//

import Foundation
import UIKit
enum StoryBoard: String {
    case Signup = "Signup"
    case Home = "Home"
    case News = "News"
    case Portfolio = "Portfolio"
    case Leftmenu = "Leftmenu"
}




extension UIViewController {
    class func instance(storyBoard: StoryBoard) -> UIViewController {
        let storyboard = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        let identifier = NSStringFromClass(self).components(separatedBy: ".").last!
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
    
    func push(viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func pop(){
        self.navigationController?.popViewController(animated: true)
    }
    func popToRoot() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    func presentOnRoot(`with` viewController : UIViewController){
        
        //self.navigationController?.modalPresentationStyle = .pageSheet
        viewController.modalPresentationStyle = .formSheet
        //viewController.modalTransitionStyle = .flipHorizontal
        
        self.present(viewController, animated: true, completion: nil)
    }
}
